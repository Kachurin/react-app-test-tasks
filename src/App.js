import React, { Component } from 'react';
import './App.css';

class XTask extends React.Component {
  constructor(props)
  {
    super(props);
    this.state={
      stateNum:0,
      unsavedChanges:null
    };
  }
  formatDateNumber(val)
  {
    return ((val>9)?"":"0")+val;
  }
  parseDateForInput(date)
  {
    return (date===null)?""
      :date.getFullYear()
      +"-"+this.formatDateNumber(date.getMonth()+1)
      +"-"+this.formatDateNumber(date.getDate())
      +"T"+this.formatDateNumber(date.getHours())
      +":"+this.formatDateNumber(date.getMinutes());
  }
  parseDateForText(date)
  {
    return (date===null)?"----"
      :date.toLocaleDateString()
      +" "+this.formatDateNumber(date.getHours())
      +":"+this.formatDateNumber(date.getMinutes());
  }
  changeCompletion()
  {
    var currentData = this.props.data;
    currentData.completion=(currentData.completion===null)?new Date():null;
    var newStateNum = this.state.stateNum+1;
    this.setState({stateNum:newStateNum});
    this.props.fonchange(this);
  }
  editData()
  {
    this.setState({unsavedChanges:{}});
  }
  saveChanges()
  {
    var changes = this.state.unsavedChanges;
    var stateUpdate = {};
    stateUpdate.stateNum=this.state.stateNum+1;
    var currentData = this.props.data;
    if (changes.title!==undefined)
       currentData.title = changes.title;
    if (changes.description!==undefined)
      currentData.description = changes.description;
    if (changes.priority!==undefined)
      currentData.priority = changes.priority;
    if (changes.deadline!==undefined)
      currentData.deadline = changes.deadline;
    if (changes.completion!==undefined)
      currentData.completion = changes.completion;
    stateUpdate.unsavedChanges = null;
    this.setState(stateUpdate);
    this.props.fonchange(this);
  }
  revokeChanges()
  {
    this.setState({unsavedChanges:null});
  }
  onInputChange(e)
  {
    var uChanges=this.state.unsavedChanges;
    var target = e.target;
    switch(target.name)
    {
      case "title":
        uChanges.title=target.value;
        break;
      case "description":
        uChanges.description=target.value;
        break;
      case "priority":
        uChanges.priority=parseInt(target.value,10);
        break;
      case "deadline":
        {
          let newDate = new Date(target.value);
          uChanges.deadline=(isNaN(newDate.getTime()))?null:newDate;
        }
        break;
      case "completion":
        {
          let newDate = new Date(target.value);
          uChanges.completion=(isNaN(newDate.getTime()))?null:newDate;
        }
        break;
      default:
        return;
    }
  }
  deleteThisTask()
  {
    this.props.fondelete(this);
  }
  renderProp(name,value)
  {
    return (
      <tr>
        <td className="PropertyName">{name}</td>
        <td className="PropertyValue">{value}</td>
      </tr>);
  }
  isOverdue()
  {
    var data = this.props.data;
    return ((data.deadline!==null)&&(data.completion===null)&&((data.deadline<new Date())));
  }
  render() {
    var isRedactMode = (this.state.unsavedChanges!==null);
    var data = this.props.data;
    var taskPriorityNames = ["обычная","важная","очень важная"];
    return(
      <div className="XTask" style={{backgroundColor: (this.isOverdue()?"red":"gray")}}>
        <div className="XKey">
          <b>№{this.props.xkey}</b>
        </div>
        <table name="DataTable">
          {this.renderProp("Название: ", (!isRedactMode)
            ?data.title
            :(<input type="text" name="title" defaultValue={data.title}
              onChange={this.onInputChange.bind(this)}/>))}
          {this.renderProp("Описание: ", (!isRedactMode)
            ?data.description
            :(<textarea className="DescriptionTextArea" name="description" defaultValue={data.description}
              onChange={this.onInputChange.bind(this)}/>))}
          {this.renderProp("Важность: ", (!isRedactMode)
            ?taskPriorityNames[data.priority]
            :(<select name="priority" defaultValue={data.priority}
              onChange={this.onInputChange.bind(this)}>
                <option value={0}>{taskPriorityNames[0]}</option>
                <option value={1}>{taskPriorityNames[1]}</option>
                <option value={2}>{taskPriorityNames[2]}</option>
              </select>))}
          {this.renderProp("Срок: ", (!isRedactMode)
            ?this.parseDateForText(data.deadline)
            :<input type="datetime-local" name="deadline" defaultValue={this.parseDateForInput(data.deadline)}
              onChange={this.onInputChange.bind(this)}/>)}
          {this.renderProp("Завершено: ", (!isRedactMode)
            ?this.parseDateForText(data.completion)
            :<input type="datetime-local" name="completion" defaultValue={this.parseDateForInput(data.completion)}
              onChange={this.onInputChange.bind(this)}/>)}
        </table>
        <div className="Buttons">
          {
            (isRedactMode)?
            [
              (<input type="button" value="Сохранить" onClick = {this.saveChanges.bind(this)}/>),
              (<input type="button" value="Отменить" onClick = {this.revokeChanges.bind(this)}/>)
            ]
            :
            [
              (<input type="button" value="Редактировать" onClick = {this.editData.bind(this)}/>),
              (<input type="button" value={((data.completion===null)?"Завершить":"Возобновить")+" выполнение"} onClick = {this.changeCompletion.bind(this)}/>),
              (<input type="button" value={"Удалить"} onClick = {this.deleteThisTask.bind(this)}/>)
            ]
          }
        </div>
      </div>
    );
  }
}

class App extends Component {
  constructor(props)
  {
    super(props);
    var appStateText = localStorage.getItem("appState");
	var newState;
    if (appStateText===null)
    {
      newState = {
        stateNum:0,
        taskCnt:0,
        tasksPriority:-1,
        commonData:new Map()
      };
      localStorage.setItem("appState",this.convertStateObjectToText(newState));
    }
    else
      newState = this.convertStateTextToObject(appStateText);
    this.state = newState;
  }
  convertStateObjectToText(obj)
  {
    var arr = [];
    arr.push(obj.stateNum,obj.taskCnt,obj.tasksPriority);
    for (var [key,value] of obj.commonData)
      arr.push(
        key,
        value.title,
        value.description,
        value.priority,
        (value.deadline===null)?null:value.deadline.getTime(),
        (value.completion===null)?null:value.completion.getTime()
      );
    var txt = JSON.stringify(arr);
    return txt;
  }
  convertStateTextToObject(txt)
  {
    var arr = JSON.parse(txt);
    var cnt = arr.length;
    var obj = {};
    obj.stateNum = arr[0];
    obj.taskCnt = arr[1];
    obj.tasksPriority = arr[2];
    var dataMap = new Map();
    obj.commonData = dataMap;
    for (var i = 3; i!==cnt; i+=6)
    {
      var data = {};
      dataMap.set(arr[i],data);
      data.title = arr[i+1];
      data.description = arr[i+2];
      data.priority = arr[i+3];
      var deadlineTime = arr[i+4];
      var completionTime = arr[i+5];
      data.deadline=((deadlineTime===null)?null:new Date(deadlineTime));
      data.completion=((completionTime===null)?null:new Date(completionTime));
    }
    return obj;
  }
  componentDidUpdate()
  {
    localStorage.setItem("appState",this.convertStateObjectToText(this.state));
  }
  renderXTask(_key,_data) {
    return <XTask
      xkey={_key}
      data={_data}
      fonchange={this.onTaskDataChange.bind(this)}
      fondelete={this.onTaskDelete.bind(this)}
    />;
  }
  addEmptyTask()
  {
    var newStateNum = this.state.stateNum+1;
    var newKey = this.state.taskCnt+1;
    var currentData = this.state.commonData;
    currentData.set(newKey,{title:"",description:"",priority:0,deadline:null,completion:null});
    this.setState({stateNum: newStateNum, taskCnt: newKey});
  }
  isVisibleByPriority(taskPriority)
  {
    return ((this.state.tasksPriority===-1)||(this.state.tasksPriority===taskPriority));
  }
  onTaskDataChange(task)
  {
    if (this.isVisibleByPriority(task.props.data.priority))
    {
      var newStateNum = this.state.stateNum+1;
      this.setState({stateNum: newStateNum});
    }
  }
  onTaskDelete(task)
  {
    var newStateNum = this.state.stateNum+1;
    var currentData = this.state.commonData;
    currentData.delete(task.props.xkey);
    this.setState({stateNum: newStateNum});
  }
  onSelectionChange(e)
  {
    this.setState({tasksPriority:parseInt(e.target.value,10)});
  }
  renderTasks()
  {
    var currentData = this.state.commonData;
    var tasks = [];
    for (var [key,value] of currentData)
      if (this.isVisibleByPriority(value.priority))
        tasks.push(this.renderXTask(key, value));
    return tasks;
  }
  eraseLocalStorageData()
  {
	localStorage.removeItem("appState");
    alert("Данные удалены.");
  }
  render() {
    return (
      <div className="App">
        <select defaultValue={this.state.tasksPriority} onChange={this.onSelectionChange.bind(this)}>
          <option value={-1}>все</option>
          <option value={0}>обычные</option>
          <option value={1}>важные</option>
          <option value={2}>очень важные</option>
        </select>
        <button onClick={this.addEmptyTask.bind(this)}>Добавить задачу</button>
        <button onClick={this.eraseLocalStorageData.bind(this)}>Удалить данные из браузера</button>
		<div><br/><b>Задачи: {this.state.commonData.size}</b></div>
        <div>
          {this.renderTasks()}
        </div>
      </div>
    );
  }
}

export default App;
